var starBurst=function (col1,col2,segments,width,height){
    "use strict";
    var can=document.createElement("canvas");
    can.height=height;
    can.width=width;
    var ctx=can.getContext("2d");
    ctx.translate(can.width/2,can.height/2);
    var getCircCoords=(r,i)=>[r*Math.sin(i),r*Math.cos(i)];
    var r=Math.pow(Math.pow(can.height,2)+Math.pow(can.width,2),0.5);
    var theta=2*Math.PI/segments;
    for(let i=0;i<segments;i++){
        ctx.strokeStyle=ctx.fillStyle=i%2?col1:col2;
        ctx.beginPath();
        ctx.moveTo(0,0);
        ctx.lineTo(...getCircCoords(r,i*theta));
        ctx.lineTo(...getCircCoords(r,(i+1)*theta));
        ctx.lineTo(0,0);
        ctx.closePath();
        ctx.fill();
        i!==segments-1&&ctx.stroke();
    }
    return can.toDataURL();
};
